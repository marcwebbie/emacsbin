# Emacs rpm binaries for Red Hat 6 workstations

## Building

````bash
git clone https://bitbucket.org/marcwebbie/emacsbin

cd emacsbin

sudo rpm -Uvh autoconf-2-1.0-1.x86_64.rpm

sudo rpm -Uvh emacs-24.4-50.x86_64.rpm
```
